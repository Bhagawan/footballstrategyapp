package com.example.footballstrategyapp.view

enum class StrategyActions {
    MOVE, CREATE_DEFAULT_ARROW, CREATE_DASH_ARROW, CREATE_NO_HEAD_ARROW, CREATE_ENEMY, DELETE_ELEMENT
}