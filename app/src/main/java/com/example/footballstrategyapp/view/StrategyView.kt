package com.example.footballstrategyapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.MotionEvent
import android.view.View
import com.example.footballstrategyapp.util.Arrow
import com.example.footballstrategyapp.util.Coordinate
import com.example.footballstrategyapp.util.Strategy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*

class StrategyView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var playerWidth = 1.0f
    private var xScale = 1.0f
    private var yScale = 1.0f

    companion object {
        const val FIELD_WIDTH = 20 //ue
        const val FIELD_HEIGHT = 7
    }

    private var strategy: Strategy = createNewStrategy()
    private var unfinishedArrow: Arrow? = null
    private var movingPlayer: Int? = null

    private var action = StrategyActions.CREATE_DASH_ARROW

    private var sInterface: StrategyInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            playerWidth = maxOf(mWidth, mHeight) / 20.0f
            xScale = mWidth / FIELD_WIDTH.toFloat()
            yScale = mHeight / FIELD_HEIGHT.toFloat()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawStrategy(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(action) {
                    StrategyActions.MOVE -> {
                        selectPlayer(event.x, event.y)
                    }
                    StrategyActions.CREATE_DEFAULT_ARROW, StrategyActions.CREATE_DASH_ARROW, StrategyActions.CREATE_NO_HEAD_ARROW -> {
                        unfinishedArrow = Arrow(noEnd = true, dash = action == StrategyActions.CREATE_DASH_ARROW || action == StrategyActions.CREATE_NO_HEAD_ARROW)
                    }
                    else -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(action) {
                    StrategyActions.MOVE -> {
                        movePLayerTo(event.x, event.y)
                    }
                    StrategyActions.CREATE_DEFAULT_ARROW, StrategyActions.CREATE_DASH_ARROW, StrategyActions.CREATE_NO_HEAD_ARROW -> {
                        unfinishedArrow?.line?.add(Coordinate(event.x / xScale, event.y / yScale))
                    }
                    else -> {}
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(action) {
                    StrategyActions.MOVE -> {
                        movingPlayer = null
                    }
                    StrategyActions.CREATE_DEFAULT_ARROW, StrategyActions.CREATE_DASH_ARROW, StrategyActions.CREATE_NO_HEAD_ARROW-> {
                        unfinishedArrow?.let {
                            if(action != StrategyActions.CREATE_NO_HEAD_ARROW) it.noEnd = false
                            strategy.arrows.add(it)
                        }
                        unfinishedArrow = null
                        sInterface?.finishAction(action)
                    }
                    StrategyActions.DELETE_ELEMENT -> {
                        if(deleteElement(event.x, event.y)) sInterface?.finishAction(action)
                    }
                    StrategyActions.CREATE_ENEMY -> {
                        strategy.enemies.add(Coordinate(event.x / xScale, event.y / yScale))
                        sInterface?.finishAction(action)
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: StrategyInterface) {
        sInterface = i
    }

    fun seAction(newAction: StrategyActions) {
        this.action = newAction
    }

    fun getStrategy() : Strategy = strategy

    fun setStrategy(newStrategy: Strategy) {
        strategy = newStrategy
    }

    //// Private

    private fun drawStrategy(c: Canvas) {
        val p = Paint()

        p.color = if(action != StrategyActions.DELETE_ELEMENT) Color.WHITE else Color.RED
        p.strokeWidth = 14.0f
        for(enemy in strategy.enemies) {
            c.drawLine(enemy.x * xScale - playerWidth * 0.4f, enemy.y * yScale- playerWidth * 0.4f, enemy.x * xScale + playerWidth * 0.4f, enemy.y * yScale + playerWidth * 0.4f, p)
            c.drawLine(enemy.x * xScale - playerWidth * 0.4f, enemy.y * yScale + playerWidth * 0.4f, enemy.x * xScale + playerWidth * 0.4f, enemy.y * yScale - playerWidth * 0.4f, p)
        }

        for(arrow in strategy.arrows) drawArrow(c, arrow)

        unfinishedArrow?.let { arrow -> drawArrow(c, arrow) }

        p.color = Color.RED
        for(player in strategy.team) {
            c.drawCircle(player.x * xScale, player.y * yScale, playerWidth / 2.0f, p)
        }
    }

    private fun drawArrow(c: Canvas, arrow: Arrow) {
        val p = Paint()
        p.strokeWidth = 6.0f
        if(arrow.line.size > 1) {
            p.color = Color.WHITE
            if(arrow.dash) {
                val maxDashLength = playerWidth / 2.0f
                var dashLength = 0.0f
                var breakPoint: Coordinate? = null
                var space = false
                for(n in 1 until arrow.line.size) {
                    var l = sqrt(((arrow.line[n].x * xScale - (breakPoint?.x ?: (arrow.line[n - 1].x * xScale)))).pow(2) + ((arrow.line[n].y * yScale - (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)))).pow(2))
                    while( dashLength + l >= maxDashLength) {
                        val newX = (breakPoint?.x ?: (arrow.line[n - 1].x * xScale)) + ((arrow.line[n].x * xScale - (breakPoint?.x ?: (arrow.line[n - 1].x * xScale)))) / l * (maxDashLength - dashLength)
                        val newY = (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)) + ((arrow.line[n].y * yScale - (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)))) / l * (maxDashLength - dashLength)
                        if(!space) c.drawLine((breakPoint?.x ?: (arrow.line[n - 1].x * xScale)), (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)), newX, newY, p)
                        breakPoint = Coordinate( newX, newY )
                        space = !space
                        dashLength = 0.0f
                        l = sqrt(((arrow.line[n].x * xScale - breakPoint.x)).pow(2) + ((arrow.line[n].y * yScale - breakPoint.y)).pow(2))
                    }
                    if(!space) {
                        c.drawLine((breakPoint?.x ?: (arrow.line[n - 1].x * xScale)), (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)),arrow.line[n].x * xScale, arrow.line[n].y * yScale, p)
                    }
                    dashLength += l
                    breakPoint = null
                }
            } else for(n in 1 until arrow.line.size) c.drawLine(arrow.line[n - 1].x * xScale, arrow.line[n - 1].y * yScale, arrow.line[n].x * xScale, arrow.line[n].y * yScale, p)

            if(!arrow.noEnd) {
                val hyp = sqrt((arrow.line.last().x - arrow.line[arrow.line.size - 2].x).pow(2) + (arrow.line.last().y - arrow.line[arrow.line.size - 2].y).pow(2))
                val angle = asin((arrow.line.last().x - arrow.line[arrow.line.size - 2].x) / hyp)

                val tTb: Boolean = if(sqrt(((arrow.line[arrow.line.size - 2].x - arrow.line.last().x).pow(2) + (arrow.line[arrow.line.size - 2].y - arrow.line.last().y).pow(2))) >= 0.25f ) {
                    arrow.line[arrow.line.size - 2].y > arrow.line.last().y
                } else {
                    var n = arrow.line.size - 2
                    while(n > 0 && (sqrt(((arrow.line[n].x - arrow.line.last().x).pow(2) + (arrow.line[n].y - arrow.line.last().y).pow(2))) < 0.25f )) {
                        n--
                    }
                    arrow.line.last().y < arrow.line[n].y
                }
                c.drawLine(arrow.line.last().x * xScale, arrow.line.last().y * yScale, (arrow.line[arrow.line.size - 2].x - sin(angle + Math.PI / 6.0f).toFloat() * 0.25f) * xScale, (arrow.line[arrow.line.size - 2].y - cos(angle + Math.PI / 6.0f).toFloat() * 0.25f * if(tTb) -1 else 1) * yScale, p)
                c.drawLine(arrow.line.last().x * xScale, arrow.line.last().y * yScale, (arrow.line[arrow.line.size - 2].x - sin(angle - Math.PI / 6.0f).toFloat() * 0.25f) * xScale, (arrow.line[arrow.line.size - 2].y - cos(angle - Math.PI / 6.0f).toFloat() * 0.25f * if(tTb) -1 else 1) * yScale, p)
            }
        }
        if(action == StrategyActions.DELETE_ELEMENT) {
            val n = arrow.line.size / 2
            p.color = Color.RED
            p.strokeWidth = 10.0f
            c.drawLine(arrow.line[n].x * xScale - playerWidth * 0.4f, arrow.line[n].y * yScale - playerWidth * 0.4f, arrow.line[n].x * xScale + playerWidth * 0.4f, arrow.line[n].y * yScale + playerWidth * 0.4f, p)
            c.drawLine(arrow.line[n].x * xScale - playerWidth * 0.4f, arrow.line[n].y * yScale + playerWidth * 0.4f, arrow.line[n].x * xScale + playerWidth * 0.4f, arrow.line[n].y * yScale - playerWidth * 0.4f, p)
        }
    }

    private fun createNewStrategy(): Strategy {
        val s = Strategy( listOf(
            Coordinate(1.0f, FIELD_HEIGHT / 2.0f),
            Coordinate(4.0f, FIELD_HEIGHT / 6.0f),
            Coordinate(4.0f, FIELD_HEIGHT / 6.0f * 2),
            Coordinate(4.0f, FIELD_HEIGHT / 6.0f * 3),
            Coordinate(4.0f, FIELD_HEIGHT / 6.0f * 4),
            Coordinate(4.0f, FIELD_HEIGHT / 6.0f * 5),
            Coordinate(8.0f, FIELD_HEIGHT / 6.0f),
            Coordinate(8.0f, FIELD_HEIGHT / 6.0f * 2),
            Coordinate(8.0f, FIELD_HEIGHT / 6.0f * 3),
            Coordinate(8.0f, FIELD_HEIGHT / 6.0f * 4),
            Coordinate(8.0f, FIELD_HEIGHT / 6.0f * 5)
        ))
        sInterface?.newStrategyCreated()
        return s
    }

    private fun selectPlayer(x: Float, y: Float) {
        for(player in strategy.team) {
            if(sqrt((player.x * xScale - x).pow(2) + (player.y * yScale - y).pow(2)) < max(playerWidth / 2.0f, 50.0f)) {
                movingPlayer = strategy.team.indexOf(player)
                break
            }
        }

    }

    private fun movePLayerTo(x: Float, y: Float) {
        if(movingPlayer != null) {
            strategy.team[movingPlayer!!].x = x / xScale
            strategy.team[movingPlayer!!].y = y / yScale
        }
    }

    private fun deleteElement(x: Float, y: Float): Boolean {
        var elementDeleted = false
        var n = 0
        while(n < strategy.arrows.size) {
            if(sqrt((strategy.arrows[n].line[strategy.arrows[n].line.size / 2].x * xScale - x).pow(2) + (strategy.arrows[n].line[strategy.arrows[n].line.size / 2].y * yScale - y).pow(2)) < max(playerWidth / 2.0f, 40.0f)) {
                strategy.arrows.removeAt(n)
                elementDeleted = true
                n--
            }
            n++
        }
        if(!elementDeleted) {
            while(n < strategy.enemies.size) {
                if(sqrt((strategy.enemies[n].x * xScale - x).pow(2) + (strategy.enemies[n].y * yScale - y).pow(2)) < max(playerWidth / 2.0f, 40.0f)) {
                    strategy.enemies.removeAt(n)
                    elementDeleted = true
                    n--
                }
                n++
            }
        }
        return elementDeleted
    }

    interface StrategyInterface {
        fun finishAction(currentAction: StrategyActions)
        fun newStrategyCreated()
    }
}