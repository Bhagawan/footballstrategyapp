package com.example.footballstrategyapp.util

import androidx.annotation.Keep

@Keep
data class FootballStrategySplashResponse(val url : String)