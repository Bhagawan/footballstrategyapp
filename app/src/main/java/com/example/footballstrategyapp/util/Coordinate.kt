package com.example.footballstrategyapp.util

data class Coordinate(var x: Float, var y: Float)
