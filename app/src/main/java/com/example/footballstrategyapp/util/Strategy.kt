package com.example.footballstrategyapp.util

import java.util.*

data class Strategy(val team: List<Coordinate>, val enemies: ArrayList<Coordinate> = ArrayList(), val arrows: ArrayList<Arrow> = ArrayList(), var name: String = "", var note: String = "", val id: String = UUID.randomUUID().toString()) {

    override fun equals(other: Any?): Boolean {
        return if(other is Strategy) other.id == this.id
        else false
    }

    override fun hashCode(): Int {
        var result = team.hashCode()
        result = 31 * result + enemies.hashCode()
        result = 31 * result + arrows.hashCode()
        result = 31 * result + note.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + name.hashCode()
        return result
    }
}
