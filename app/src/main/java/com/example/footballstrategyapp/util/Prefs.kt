package com.example.footballstrategyapp.util

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class Prefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("FootballStrategyApp", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        fun loadSavedStrategies(context: Context): List<Strategy> {
            val shP = context.getSharedPreferences("FootballStrategyApp", Context.MODE_PRIVATE)
            val g = Gson()
            val type = object : TypeToken<List<Strategy>>() {}.type
            return g.fromJson(shP.getString("savedStrategies", "[]"), type)
        }

        fun saveStrategy(context: Context, strategy: Strategy) {
            val shP = context.getSharedPreferences("FootballStrategyApp", Context.MODE_PRIVATE)
            val g = Gson()
            val type = object : TypeToken<List<Strategy>>() {}.type
            val strategies =  ArrayList(g.fromJson<List<Strategy>>(shP.getString("savedStrategies", "[]"), type))

            var edit = false
            for(n in strategies.indices) {
                if(strategies[n] == strategy) {
                    edit = true
                    strategies[n] = strategy
                }
            }
            if(!edit) strategies.add(strategy)
            shP.edit().putString("savedStrategies", g.toJson(strategies.toList())).apply()
        }

        fun deleteStrategy(context: Context, strategy: Strategy) {
            val shP = context.getSharedPreferences("FootballStrategyApp", Context.MODE_PRIVATE)
            val g = Gson()
            val type = object : TypeToken<List<Strategy>>() {}.type
            val strategies =  ArrayList(g.fromJson<List<Strategy>>(shP.getString("savedStrategies", "[]"), type))
            strategies.remove(strategy)
            shP.edit().putString("savedStrategies", g.toJson(strategies)).apply()
        }
    }
}