package com.example.footballstrategyapp.util

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.example.footballstrategyapp.view.StrategyView
import kotlin.math.*

class Snapshot {
    companion object {

        fun createSnapshot(strategy: Strategy, width: Int, height: Int, back: Bitmap?): Bitmap {
            val snapshot = if(back!= null) Bitmap.createScaledBitmap(back, width, height, false)
            else Bitmap.createBitmap(width, height,  Bitmap.Config.ARGB_8888)
            val canvas = Canvas(snapshot)
            if(back == null) canvas.drawColor(Color.parseColor("FF363F2C"))
            drawStrategy(canvas, strategy)
            return snapshot
        }

        private fun drawStrategy(c: Canvas, strategy: Strategy) {
            val p = Paint()
            val playerWidth = maxOf(c.width, c.height) / 20.0f
            val xScale = c.width / StrategyView.FIELD_WIDTH.toFloat()
            val yScale = c.height / StrategyView.FIELD_HEIGHT.toFloat()

            p.color = Color.WHITE
            p.strokeWidth = 14.0f
            for(enemy in strategy.enemies) {
                c.drawLine(enemy.x * xScale - playerWidth * 0.4f, enemy.y * yScale- playerWidth * 0.4f, enemy.x * xScale + playerWidth * 0.4f, enemy.y * yScale + playerWidth * 0.4f, p)
                c.drawLine(enemy.x * xScale - playerWidth * 0.4f, enemy.y * yScale + playerWidth * 0.4f, enemy.x * xScale + playerWidth * 0.4f, enemy.y * yScale - playerWidth * 0.4f, p)
            }

            for(arrow in strategy.arrows) drawArrow(c, arrow)

            p.color = Color.RED
            for(player in strategy.team) {
                c.drawCircle(player.x * xScale, player.y * yScale, playerWidth / 2.0f, p)
            }
        }

        private fun drawArrow(c: Canvas, arrow: Arrow) {
            val p = Paint()
            val playerWidth = maxOf(c.width, c.height) / 20.0f
            val xScale = c.width / StrategyView.FIELD_WIDTH.toFloat()
            val yScale = c.height / StrategyView.FIELD_HEIGHT.toFloat()
            p.strokeWidth = 6.0f
            if(arrow.line.size > 1) {
                p.color = Color.WHITE
                if(arrow.dash) {
                    val maxDashLength = playerWidth / 2.0f
                    var dashLength = 0.0f
                    var breakPoint: Coordinate? = null
                    var space = false
                    for(n in 1 until arrow.line.size) {
                        var l = sqrt(((arrow.line[n].x * xScale - (breakPoint?.x ?: (arrow.line[n - 1].x * xScale)))).pow(2) + ((arrow.line[n].y * yScale - (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)))).pow(2))
                        while( dashLength + l >= maxDashLength) {
                            val newX = (breakPoint?.x ?: (arrow.line[n - 1].x * xScale)) + ((arrow.line[n].x * xScale - (breakPoint?.x ?: (arrow.line[n - 1].x * xScale)))) / l * (maxDashLength - dashLength)
                            val newY = (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)) + ((arrow.line[n].y * yScale - (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)))) / l * (maxDashLength - dashLength)
                            if(!space) c.drawLine((breakPoint?.x ?: (arrow.line[n - 1].x * xScale)), (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)), newX, newY, p)
                            breakPoint = Coordinate( newX, newY )
                            space = !space
                            dashLength = 0.0f
                            l = sqrt(((arrow.line[n].x * xScale - breakPoint.x)).pow(2) + ((arrow.line[n].y * yScale - breakPoint.y)).pow(2))
                        }
                        if(!space) {
                            c.drawLine((breakPoint?.x ?: (arrow.line[n - 1].x * xScale)), (breakPoint?.y ?: (arrow.line[n - 1].y * yScale)),arrow.line[n].x * xScale, arrow.line[n].y * yScale, p)
                        }
                        dashLength += l
                        breakPoint = null
                    }
                } else for(n in 1 until arrow.line.size) c.drawLine(arrow.line[n - 1].x * xScale, arrow.line[n - 1].y * yScale, arrow.line[n].x * xScale, arrow.line[n].y * yScale, p)

                if(!arrow.noEnd) {
                    val hyp = sqrt((arrow.line.last().x - arrow.line[arrow.line.size - 2].x).pow(2) + (arrow.line.last().y - arrow.line[arrow.line.size - 2].y).pow(2))
                    val angle = asin((arrow.line.last().x - arrow.line[arrow.line.size - 2].x) / hyp)

                    val tTb: Boolean = if(sqrt(((arrow.line[arrow.line.size - 2].x - arrow.line.last().x).pow(2) + (arrow.line[arrow.line.size - 2].y - arrow.line.last().y).pow(2))) >= 0.25f ) {
                        arrow.line[arrow.line.size - 2].y > arrow.line.last().y
                    } else {
                        var n = arrow.line.size - 2
                        while(n > 0 && (sqrt(((arrow.line[n].x - arrow.line.last().x).pow(2) + (arrow.line[n].y - arrow.line.last().y).pow(2))) < 0.25f )) {
                            n--
                        }
                        arrow.line.last().y < arrow.line[n].y
                    }
                    c.drawLine(arrow.line.last().x * xScale, arrow.line.last().y * yScale, (arrow.line[arrow.line.size - 2].x - sin(angle + Math.PI / 6.0f).toFloat() * 0.25f) * xScale, (arrow.line[arrow.line.size - 2].y - cos(angle + Math.PI / 6.0f).toFloat() * 0.25f * if(tTb) -1 else 1) * yScale, p)
                    c.drawLine(arrow.line.last().x * xScale, arrow.line.last().y * yScale, (arrow.line[arrow.line.size - 2].x - sin(angle - Math.PI / 6.0f).toFloat() * 0.25f) * xScale, (arrow.line[arrow.line.size - 2].y - cos(angle - Math.PI / 6.0f).toFloat() * 0.25f * if(tTb) -1 else 1) * yScale, p)
                }
            }
        }
    }
}