package com.example.footballstrategyapp.util

import android.app.Activity
import android.content.pm.ActivityInfo
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.footballstrategyapp.MainViewModel
import com.example.footballstrategyapp.ui.screens.*
import com.example.footballstrategyapp.ui.screens.strategyScreen.StrategyScreen
import im.delight.android.webview.AdvancedWebView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView) {
    val viewModel = viewModel<MainViewModel>(LocalContext.current as ComponentActivity)
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.ASSETS_LOADING_ERROR_SCREEN.label) {
            BackHandler(true) {}
            ErrorScreen(viewModel::retryDownload)
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if(webView.onBackPressed()) exitProcess(0)
            }
            WebViewScreen(webView, remember { viewModel.url })
        }
        composable(Screens.MAIN_MENU_SCREEN.label) {
            BackHandler(true) {}
            MainMenuScreen()
        }
        composable(Screens.STRATEGY_SELECTION_SCREEN.label) {
            BackHandler(true) {}
            StrategySelectionScreen()
        }
        composable(Screens.STRATEGY_SCREEN.label) {
            BackHandler(true) {}
            StrategyScreen()
        }
    }
    val currActivity = LocalContext.current as Activity
    LaunchedEffect("nav") {
        viewModel.navigationFlow.onEach {
            if(it == Screens.STRATEGY_SCREEN || it == Screens.STRATEGY_SELECTION_SCREEN) {
                if(viewModel.getOriginalOrientation() == null) viewModel.setOriginalOrientation(currActivity.requestedOrientation)
                currActivity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            } else if(navController.currentDestination?.route == Screens.STRATEGY_SCREEN.label ||
                        navController.currentDestination?.route == Screens.STRATEGY_SELECTION_SCREEN.label) {
                        viewModel.getOriginalOrientation()?.let { orientation ->
                            currActivity.requestedOrientation = orientation
                            viewModel.setOriginalOrientation(null)
                    }
            }
            navController.navigate(it.label) { launchSingleTop = true }
        }.launchIn(this)
    }
    navController.enableOnBackPressed(true)
}