package com.example.footballstrategyapp.util

data class Arrow(val dash: Boolean = false, var noEnd: Boolean = false, val line: ArrayList<Coordinate> = ArrayList())
