package com.example.footballstrategyapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


val Green = Color(0xFF549704)
val Green_light = Color(0xFF798D61)
val Green_dark = Color(0xFF363F2C)
val Orange_light = Color(0xFFBD7D1F)
val Orange_dark = Color(0xFF413018)
val Red = Color(0xFFC24F47)
val Red_dark = Color(0xFF6D2A25)
val Grey_light = Color(0xFF6B6B6B)
val Grey = Color(0xFF4B4B4B)
val Grey_transparent = Color(0x804B4B4B)