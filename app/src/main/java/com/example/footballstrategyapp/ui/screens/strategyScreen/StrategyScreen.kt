package com.example.footballstrategyapp.ui.screens.strategyScreen

import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.footballstrategyapp.MainViewModel
import com.example.footballstrategyapp.util.Prefs
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun StrategyScreen() {
    val context = LocalContext.current
    val viewModel = viewModel<MainViewModel>(LocalContext.current as ComponentActivity)
    val strategyViewModel = viewModel<StrategyViewModel>()
    if(strategyViewModel.currentStrategy == null && viewModel.getStrategyToEdit() != null) {
        strategyViewModel.currentStrategy = viewModel.getStrategyToEdit()
    }

    LaunchedEffect("exitListener") {
        strategyViewModel.exitFlow.onEach { if(it) {
            viewModel.switchToMAinMenu()
        } }.launchIn(this)
    }
    LaunchedEffect("exitListener") {
        strategyViewModel.saveFlow.onEach { if(it) {
            strategyViewModel.currentStrategy?.let { it1 -> Prefs.saveStrategy(context, it1) }
        } }.launchIn(this)
    }

    Row(modifier = Modifier.fillMaxSize()) {
        Box(modifier = Modifier
            .weight(9.0f)
            .fillMaxSize()) {
            StrategyField()
        }
        Box(modifier = Modifier
            .weight(1.0f)
            .fillMaxSize()) {
            StrategyMenu()
        }
    }

    val showEdit = strategyViewModel.editFlow.collectAsState(initial = false)
    if(showEdit.value)  StrategyEditNotePopup()
}
