package com.example.footballstrategyapp.ui.screens

import androidx.activity.ComponentActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.footballstrategyapp.MainViewModel
import com.example.footballstrategyapp.R
import com.example.footballstrategyapp.ui.theme.Green_light
import com.example.footballstrategyapp.ui.theme.Orange_light
import com.example.footballstrategyapp.ui.theme.Red
import com.example.footballstrategyapp.util.Prefs
import kotlin.system.exitProcess

@Composable
fun MainMenuScreen() {
    val viewModel = viewModel<MainViewModel>(LocalContext.current as ComponentActivity)
    val context = LocalContext.current
    val back = remember { mutableStateOf(viewModel.field) }
    val portrait = LocalConfiguration.current.orientation == android.content.res.Configuration.ORIENTATION_PORTRAIT
    Box(modifier = Modifier
        .fillMaxSize()
        .background(color = Green_light)) {
        back.value?.let {
            Image(it.asImageBitmap(), contentDescription = "field", modifier = Modifier.fillMaxSize(), contentScale = if(portrait) ContentScale.FillHeight else ContentScale.FillBounds)
        }
        Column(modifier = Modifier
            .fillMaxSize()) {
            Box(modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f), contentAlignment = Alignment.Center) {
                Button(shape = RoundedCornerShape(Dp(20.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Orange_light),
                    contentPadding = PaddingValues(Dp(20.0f)),
                    onClick = {
                        viewModel.setStrategyToEdit(null)
                        viewModel.switchToStrategyScreen()
                    }) {
                    Text(text = stringResource(id = R.string.btn_menu_new), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp))
                }
            }
            Box(modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f), contentAlignment = Alignment.Center) {
                Button(shape = RoundedCornerShape(Dp(20.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Orange_light),
                    contentPadding = PaddingValues(Dp(20.0f)),
                    onClick = {
                        viewModel.setSavedStrategies(Prefs.loadSavedStrategies(context))
                        viewModel.selectStrategy()
                    }) {
                    Text(text = stringResource(id = R.string.btn_menu_load), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp))
                }
            }
            Box(modifier = Modifier
                .fillMaxWidth()
                .padding(Dp(10.0f)), contentAlignment = Alignment.Center) {
                Button(shape = RoundedCornerShape(Dp(20.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Orange_light),
                    contentPadding = PaddingValues(Dp(20.0f)),
                    onClick = { exitProcess(0) }) {
                    Text(text = stringResource(id = R.string.btn_menu_exit), color = Red, fontSize = TextUnit(20.0f, TextUnitType.Sp))
                }
            }
        }
    }
}

