package com.example.footballstrategyapp.ui.screens.strategyScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.footballstrategyapp.R
import com.example.footballstrategyapp.ui.theme.*

@Composable
fun StrategyEditNotePopup() {
    val viewModel = viewModel<StrategyViewModel>()
    var text by remember { mutableStateOf(viewModel.currentStrategy?.note ?: "") }
    var name by remember { mutableStateOf(viewModel.currentStrategy?.name ?: "") }
    val focusManager = LocalFocusManager.current

    Box(modifier = Modifier
        .fillMaxSize()
        .background(color = Grey_transparent)
        .clickable { focusManager.clearFocus() }, contentAlignment = Alignment.Center) {
        Column(modifier = Modifier
            .wrapContentSize()
            .padding(Dp(10.0f))
            .background(color = Grey, shape = RoundedCornerShape(Dp(15.0f))),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(text = stringResource(id = R.string.strategy_save_name), fontSize = TextUnit(20.0f, TextUnitType.Sp), color = Color.White)
            TextField(value = name,
                onValueChange = { name = it },
                singleLine = true,
                shape = RoundedCornerShape(Dp(15.0f)),
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Grey_light,
                    textColor = Color.White,
                    cursorColor = Color.White,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent),
                modifier = Modifier
                    .widthIn(min = Dp(100.0f), max = Dp(400.0f))
                    .height(Dp(50.0f))
                    .padding(Dp(1.0f)))
            Text(text = stringResource(id = R.string.strategy_save_header), fontSize = TextUnit(20.0f, TextUnitType.Sp), color = Color.White)
            TextField(value = text,
                onValueChange = { text = it },
                maxLines = 10,
                shape = RoundedCornerShape(Dp(15.0f)),
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Grey_light, textColor = Color.White, cursorColor = Color.White,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent),
                modifier = Modifier
                    .widthIn(min = Dp(100.0f), max = Dp(400.0f))
                    .heightIn(min = Dp(100.0f), max = Dp(300.0f))
                    .padding(Dp(10.0f)))
            Row(modifier = Modifier
                .wrapContentSize()
                .sizeIn(maxWidth = Dp(150.0f), maxHeight = Dp(50.0f))
                .padding(Dp(10.0f)), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween ) {
                Button(onClick = viewModel::finishEditingNote,
                    shape = RoundedCornerShape(Dp(15.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Red, contentColor = Color.White),
                    modifier = Modifier.weight(1.0f, true).padding(end = Dp(5.0f)), contentPadding = PaddingValues(Dp(1.0f))) {
                    Icon(painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24), contentDescription = stringResource(id = R.string.desc_strategy_exit),
                        modifier = Modifier
                            .fillMaxHeight(0.9f)
                            .aspectRatio(1f))
                }
                Button(onClick = {
                    viewModel.changeNote(text)
                    viewModel.changeName(name)
                    viewModel.saveStrategy()
                    viewModel.finishEditingNote()
                },
                    shape = RoundedCornerShape(Dp(15.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Green_dark, contentColor = Color.White),
                    modifier = Modifier.weight(1.0f, true).padding(start = Dp(5.0f)), contentPadding = PaddingValues(Dp(1.0f))) {
                    Icon(painter = painterResource(id = R.drawable.ic_baseline_save_24), contentDescription = stringResource(id = R.string.desc_strategy_exit),
                        modifier = Modifier
                            .fillMaxHeight(0.9f)
                            .aspectRatio(1f))
                }
            }
        }
    }
}