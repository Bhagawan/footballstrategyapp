package com.example.footballstrategyapp.ui.screens.strategyScreen

import androidx.activity.ComponentActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.footballstrategyapp.MainViewModel
import com.example.footballstrategyapp.view.StrategyActions
import com.example.footballstrategyapp.view.StrategyView

@Composable
fun StrategyField() {
    val viewModel = viewModel<MainViewModel>(LocalContext.current as ComponentActivity)
    val strategyViewModel = viewModel<StrategyViewModel>()
    val back = remember { mutableStateOf(viewModel.field) }
    val currentAction by strategyViewModel.action.observeAsState(StrategyActions.MOVE)
    Box(modifier = Modifier.fillMaxWidth()) {
        back.value?.let {
            Image(it.asImageBitmap(), contentDescription = "field", contentScale = ContentScale.FillHeight)
        }
        AndroidView(factory = { StrategyView(it) },
            modifier = Modifier.fillMaxSize(),
            update = {
                if (strategyViewModel.currentStrategy == null)  strategyViewModel.currentStrategy = it.getStrategy()
                else it.setStrategy(strategyViewModel.currentStrategy!!)
                it.seAction(currentAction)
                it.setInterface(object : StrategyView.StrategyInterface {
                    override fun finishAction(currentAction: StrategyActions) {
                        strategyViewModel.finishAction()
                    }
                    override fun newStrategyCreated() {
                        strategyViewModel.currentStrategy = it.getStrategy()
                    }
                })
            })
    }
}