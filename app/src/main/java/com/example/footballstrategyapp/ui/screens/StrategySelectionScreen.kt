package com.example.footballstrategyapp.ui.screens

import androidx.activity.ComponentActivity
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.ScrollableDefaults
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.footballstrategyapp.MainViewModel
import com.example.footballstrategyapp.R
import com.example.footballstrategyapp.ui.theme.*
import com.example.footballstrategyapp.util.Prefs
import com.example.footballstrategyapp.util.Snapshot
import com.example.footballstrategyapp.util.Strategy

@Preview
@Composable
fun StrategySelectionScreen() {
    val viewModel = viewModel<MainViewModel>(LocalContext.current as ComponentActivity)
    val back = remember { mutableStateOf(viewModel.field) }
    var strategies by remember { mutableStateOf(viewModel.getStrategiesList()) }
    var selectedStrategy by remember { mutableStateOf<Strategy?>(null)}
    var previewWidth by remember { mutableStateOf(100.0f) }
    val context = LocalContext.current

    Box(modifier = Modifier
        .fillMaxSize()
        .onGloballyPositioned { previewWidth = it.size.width / 3.5f }
        .background(color = Green_light)) {
        back.value?.let {
            Image(it.asImageBitmap(), contentDescription = "field", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
        }
        Column(modifier = Modifier
            .fillMaxSize()
            .padding(Dp(10.0f))
            .background(color = Grey, shape = RoundedCornerShape(Dp(15.0f)))
            .border(width = Dp(5.0f), color = Green_light, shape = RoundedCornerShape(Dp(15.0f)))
            , horizontalAlignment = Alignment.CenterHorizontally
            , verticalArrangement = Arrangement.Center) {
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(Dp(10.0f))) {
                Button(onClick = { viewModel.switchToMAinMenu() }
                    , contentPadding = PaddingValues(Dp(3.0f))
                    , colors = ButtonDefaults.buttonColors(backgroundColor = Red, contentColor = Color.White)
                    , modifier = Modifier
                        .size(Dp(60.0f))
                        .padding(Dp(10.0f))
                        .align(Alignment.CenterVertically)) {
                    Icon(painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24), contentDescription = stringResource(id = R.string.desc_strategy_exit),
                        modifier = Modifier
                            .background(color = Red, shape = RoundedCornerShape(Dp(15.0f)))
                            .fillMaxHeight(0.9f)
                            .aspectRatio(1f))
                }
                Button(onClick = {
                                    viewModel.setStrategyToEdit(null)
                                    viewModel.switchToStrategyScreen()
                                 }
                    , contentPadding = PaddingValues(Dp(3.0f))
                    , colors = ButtonDefaults.buttonColors(backgroundColor = Green, contentColor = Color.White)
                    , modifier = Modifier
                        .weight(1.0f, true)
                        .height(Dp(60.0f))
                        .padding(Dp(5.0f))
                        .align(Alignment.CenterVertically)) {
                    Text(text = stringResource(id = R.string.btn_menu_new), color = Color.White, fontSize = TextUnit(20.0f, TextUnitType.Sp),
                        textAlign = TextAlign.Center)
                }
            }
            Row(modifier = Modifier
                .fillMaxSize()
                .padding(Dp(15.0f))) {
                Column(modifier = Modifier
                    .weight(1.0f, true)
                    .fillMaxHeight()
                    .background(color = Grey_light, shape = RoundedCornerShape(Dp(10.0f)))
                    .padding(end = Dp(5.0f))
                    .verticalScroll(remember {
                        ScrollState(0)
                    }, enabled = true, flingBehavior = ScrollableDefaults.flingBehavior())) {
                    for(strategy in strategies) {
                        Box(modifier = Modifier
                            .padding(Dp(5.0f))
                            .fillMaxWidth()
                            .clickable { selectedStrategy = strategy }
                            .height(Dp(55.0f))
                            .background(
                                color = if (strategy == selectedStrategy) Orange_dark else Orange_light,
                                shape = RoundedCornerShape(Dp(10.0f))
                            ), contentAlignment = Alignment.CenterStart) {
                            Text(text = strategy.name
                                , fontSize = TextUnit(15.0f
                                , TextUnitType.Sp)
                                , color = Color.White
                                , textAlign = TextAlign.Center
                                , modifier = Modifier
                                    .padding(start = Dp(10.0f)))
                        }
                    }
                }
                Column(modifier = Modifier
                    .weight(1.0f, true)
                    .fillMaxHeight()
                    .padding(start = Dp(5.0f))
                    .background(color = Grey_light, shape = RoundedCornerShape(Dp(10.0f)))
                    , verticalArrangement = Arrangement.SpaceBetween
                    , horizontalAlignment = Alignment.CenterHorizontally) {
                    selectedStrategy?.let {
                        Text(text = it.name, fontSize = TextUnit(20.0f, TextUnitType.Sp), color = Color.White, textAlign = TextAlign.Center )
                        Divider(color = Grey, thickness = Dp(2.0f), modifier = Modifier.padding(vertical = Dp(2.0f), horizontal = Dp(previewWidth / 5.0f)))
                        Image(painter = BitmapPainter(Snapshot.createSnapshot(it, previewWidth.toInt(), (previewWidth* 0.75f).toInt(), viewModel.field).asImageBitmap()), contentDescription = stringResource(
                            id = R.string.desc_strategy_preview
                        ), modifier = Modifier.weight(1.0f)
                            .fillMaxHeight()
                            .aspectRatio(1f))
                        Divider(color = Grey, thickness = Dp(2.0f), modifier = Modifier.padding(vertical = Dp(2.0f), horizontal = Dp(previewWidth / 5.0f)))
                        Text(text = it.note, fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White, textAlign = TextAlign.Center
                            , modifier = Modifier
                                .padding(horizontal = Dp(10.0f))
                                .fillMaxWidth()
                                .weight(1.0f)
                                .background(color = Grey, shape = RoundedCornerShape(Dp(10.0f))))
                        Row(modifier = Modifier.fillMaxWidth().weight(0.75f).padding(Dp((3.0f))), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
                            Button(
                                onClick = {
                                    Prefs.deleteStrategy(context, it)
                                    strategies = ArrayList(strategies).minus(it).toList()
                                    selectedStrategy = null
                                },
                                shape = RoundedCornerShape(Dp(15.0f)),
                                colors = ButtonDefaults.buttonColors(backgroundColor = Red, contentColor = Color.White),
                                modifier = Modifier.weight(1.0f, true).padding(end = Dp(5.0f)), contentPadding = PaddingValues(Dp(1.0f))) {
                                Icon(painter = painterResource(id = R.drawable.ic_baseline_delete_forever_24), contentDescription = stringResource(id = R.string.desc_strategy_delete),
                                    modifier = Modifier
                                        .fillMaxHeight(0.9f)
                                        .aspectRatio(1f))
                            }
                            Button(
                                onClick = {
                                    viewModel.setStrategyToEdit(it)
                                    viewModel.switchToStrategyScreen()
                                },
                                shape = RoundedCornerShape(Dp(15.0f)),
                                colors = ButtonDefaults.buttonColors(backgroundColor = Green, contentColor = Color.White),
                                modifier = Modifier.weight(1.0f, true).padding(start = Dp(5.0f)), contentPadding = PaddingValues(Dp(1.0f))) {
                                Icon(painter = painterResource(id = R.drawable.ic_baseline_edit_note_24), contentDescription = stringResource(id = R.string.desc_strategy_edit),
                                    modifier = Modifier
                                        .fillMaxHeight(0.9f)
                                        .aspectRatio(1f))
                            }
                        }
                    }
                }
            }
        }
    }
}