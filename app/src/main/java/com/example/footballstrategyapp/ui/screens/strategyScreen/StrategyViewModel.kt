package com.example.footballstrategyapp.ui.screens.strategyScreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.footballstrategyapp.util.Strategy
import com.example.footballstrategyapp.view.StrategyActions
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class StrategyViewModel: ViewModel() {
    private val _mutableActionFlow = MutableSharedFlow<StrategyActions>()
    val actionFlow : SharedFlow<StrategyActions> = _mutableActionFlow.asSharedFlow()

    private val _exitFlow = MutableSharedFlow<Boolean>()
    val exitFlow : SharedFlow<Boolean> = _exitFlow.asSharedFlow()

    private val _editFlow = MutableStateFlow(false)
    val editFlow : StateFlow<Boolean> = _editFlow.asStateFlow()

    private val _saveFlow = MutableSharedFlow<Boolean>()
    val saveFlow : SharedFlow<Boolean> = _saveFlow.asSharedFlow()

    private var _action = MutableLiveData(StrategyActions.MOVE)
    var action : LiveData<StrategyActions> = _action

    var currentStrategy: Strategy? = null


    fun changeAction(newAction: StrategyActions) {
        viewModelScope.launch {
            if(action.value == newAction) _action.postValue(StrategyActions.MOVE)
            else _action.postValue(newAction)
        }
    }

    fun finishAction() {
        viewModelScope.launch {
            _action.postValue(StrategyActions.MOVE)
        }
    }

    fun exit() {
        viewModelScope.launch {
            _exitFlow.emit(true)
        }
    }

    fun editNote() {
        viewModelScope.launch {
            _editFlow.emit(true)
        }
    }

    fun finishEditingNote() {
        viewModelScope.launch {
            _editFlow.emit(false)
        }
    }

    fun saveStrategy() {
        viewModelScope.launch {
            _saveFlow.emit(true)
        }
    }

    fun changeNote(newNote: String) {
        currentStrategy?.note = newNote
    }
    fun changeName(newName: String) {
        currentStrategy?.name = newName
    }
}