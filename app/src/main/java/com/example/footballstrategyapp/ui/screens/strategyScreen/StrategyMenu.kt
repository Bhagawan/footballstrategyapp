package com.example.footballstrategyapp.ui.screens.strategyScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.footballstrategyapp.R
import com.example.footballstrategyapp.ui.theme.*
import com.example.footballstrategyapp.view.StrategyActions

@Composable
fun StrategyMenu() {
    val viewModel = viewModel<StrategyViewModel>()
    val action by viewModel.action.observeAsState(StrategyActions.MOVE)
    Column(modifier = Modifier
        .fillMaxSize()
        .border(width = Dp(2.0f), color = Orange_light)
        .background(color = Grey_light), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.SpaceBetween) {
        Button(onClick = { viewModel.exit() }, colors = ButtonDefaults.buttonColors(backgroundColor = Red, contentColor = Color.White), modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .padding(Dp(2.0f))
            .align(Alignment.CenterHorizontally)) {
            Icon(painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24), contentDescription = stringResource(id = R.string.desc_strategy_exit),
                modifier = Modifier
                    .background(color = Red, shape = RoundedCornerShape(Dp(15.0f)))
                    .fillMaxHeight(0.9f)
                    .aspectRatio(1f))
        }
        Button(onClick = { viewModel.saveStrategy() }, colors = ButtonDefaults.buttonColors(backgroundColor = Green_dark, contentColor = Color.White), modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .padding(Dp(2.0f))
            .align(Alignment.CenterHorizontally)) {
            Icon(painter = painterResource(id = R.drawable.ic_baseline_save_24), contentDescription = stringResource(id = R.string.desc_strategy_save),
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .aspectRatio(1f))
        }
        Button(onClick = { viewModel.editNote() }, colors = ButtonDefaults.buttonColors(backgroundColor = Orange_dark, contentColor = Color.White), modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .align(Alignment.CenterHorizontally)) {
            Icon(painter = painterResource(id = R.drawable.ic_baseline_edit_note_24), contentDescription = stringResource(id = R.string.desc_strategy_edit_note),
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .aspectRatio(1f))
        }
        Button(onClick = { viewModel.changeAction(StrategyActions.CREATE_DEFAULT_ARROW) }, colors = ButtonDefaults.buttonColors(backgroundColor = if (action == StrategyActions.CREATE_DEFAULT_ARROW) Orange_light else Orange_dark, contentColor = Color.White), modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .align(Alignment.CenterHorizontally)) {
            Icon(painter = painterResource(id = R.drawable.ic_arrow_def), contentDescription = stringResource(id = R.string.desc_strategy_arrow_def),
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .aspectRatio(1f))
        }
        Button(onClick = { viewModel.changeAction(StrategyActions.CREATE_DASH_ARROW) }, colors = ButtonDefaults.buttonColors(backgroundColor = if (action == StrategyActions.CREATE_DASH_ARROW) Orange_light else Orange_dark, contentColor = Color.White), modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .align(Alignment.CenterHorizontally)) {
            Icon(painter = painterResource(id = R.drawable.ic_arrow_dash), contentDescription = stringResource(id = R.string.desc_strategy_arrow_dash),
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .aspectRatio(1f))
        }
        Button(onClick = { viewModel.changeAction(StrategyActions.CREATE_NO_HEAD_ARROW) }, colors = ButtonDefaults.buttonColors(backgroundColor = if (action == StrategyActions.CREATE_NO_HEAD_ARROW) Orange_light else Orange_dark, contentColor = Color.White), modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .align(Alignment.CenterHorizontally)) {
            Icon(painter = painterResource(id = R.drawable.ic_arrow_no_head), contentDescription = stringResource(id = R.string.desc_strategy_arrow_no_head),
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .aspectRatio(1f))
        }
        Button(onClick = { viewModel.changeAction(StrategyActions.CREATE_ENEMY) }, colors = ButtonDefaults.buttonColors(backgroundColor = if (action == StrategyActions.CREATE_ENEMY) Orange_light else Orange_dark, contentColor = Color.White), modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .align(Alignment.CenterHorizontally)) {
            Icon(painter = painterResource(id = R.drawable.ic_cross), contentDescription = stringResource(id = R.string.desc_strategy_enemy),
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .aspectRatio(1f))
        }
        Button(onClick = { viewModel.changeAction(StrategyActions.DELETE_ELEMENT) }, colors = ButtonDefaults.buttonColors(backgroundColor = if (action == StrategyActions.DELETE_ELEMENT) Orange_light else Orange_dark), modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true)
            .align(Alignment.CenterHorizontally)) {
            Icon(painter = painterResource(id = R.drawable.ic_baseline_delete_forever_24), tint = Red, contentDescription = stringResource(id = R.string.desc_strategy_delete),
                modifier = Modifier
                    .fillMaxHeight(0.9f)
                    .aspectRatio(1f))
        }
    }
}
