package com.example.footballstrategyapp.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    ASSETS_LOADING_ERROR_SCREEN("assets_loading_error"),
    WEB_VIEW("web_view"),
    MAIN_MENU_SCREEN("main_menu"),
    STRATEGY_SELECTION_SCREEN("strategy_selection_screen"),
    STRATEGY_SCREEN("strategy_screen")
}