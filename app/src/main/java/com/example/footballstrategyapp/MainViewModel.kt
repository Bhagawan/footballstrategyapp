package com.example.footballstrategyapp

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.footballstrategyapp.ui.screens.Screens
import com.example.footballstrategyapp.util.FootballStrategyServerClient
import com.example.footballstrategyapp.util.Strategy
import com.example.footballstrategyapp.util.UrlField
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class MainViewModel: ViewModel() {
    private var request: Job? = null
    private var fieldDownload: Job? = null

    private val _navigationFlow = MutableSharedFlow<Screens>( 1)
    val navigationFlow = _navigationFlow.asSharedFlow()

    var url = ""
    var field : Bitmap? = null

    private var strategyToEdit: Strategy? = null
    private var originalScreenOrientation: Int? = null
    private var strategiesArray: List<Strategy>? = null

    init {
        Log.d("MainViewModelCreated: ", "--------------------------------- " + hashCode().toString())
    }

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun retryDownload() {
        _navigationFlow.tryEmit(Screens.SPLASH_SCREEN)
        switchToMainApp()
    }

    fun selectStrategy() {
        if(field != null) {
            viewModelScope.launch {
                _navigationFlow.tryEmit(Screens.STRATEGY_SELECTION_SCREEN)
            }
        } else switchToErrorScreen()
    }

    fun switchToStrategyScreen() {
        if(field != null) {
            viewModelScope.launch {
                _navigationFlow.tryEmit(Screens.STRATEGY_SCREEN)
            }
        } else switchToErrorScreen()
    }

    fun switchToMAinMenu() {
        viewModelScope.launch {
            _navigationFlow.tryEmit(Screens.MAIN_MENU_SCREEN)
        }
    }

    fun setOriginalOrientation(orientation: Int?) {
        originalScreenOrientation = orientation
    }

    fun getOriginalOrientation() : Int? = originalScreenOrientation

    fun setStrategyToEdit(strategy: Strategy?) {
        strategyToEdit = strategy
    }

    fun getStrategiesList(): List<Strategy> = strategiesArray ?: emptyList()

    fun setSavedStrategies(list: List<Strategy>) {
        strategiesArray = list
    }

    fun getStrategyToEdit(): Strategy? = strategyToEdit

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = FootballStrategyServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToMainApp()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToMainApp()
                            }
                            else -> viewModelScope.launch {
                                url ="https://${splash.body()!!.url}"
                                _navigationFlow.tryEmit(Screens.WEB_VIEW)
                            }
                        }
                    } else switchToMainApp()
                } else switchToMainApp()
            }
        } catch (e: Exception) {
            switchToMainApp()
        }
    }

    private lateinit var tar: Target
    private fun switchToMainApp() {
        if(fieldDownload?.isActive == true) {
            fieldDownload?.cancel()
        }
        fieldDownload = viewModelScope.launch {
            val picasso = Picasso.get()

            tar = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    switchToErrorScreen()
                }
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if(bitmap != null) {
                        field = bitmap
                        switchToMenu()
                    } else {
                        switchToErrorScreen()
                    }
                }
            }
            picasso.load(UrlField).into(tar)
        }
    }

    private fun switchToMenu() {
        viewModelScope.launch {
            _navigationFlow.tryEmit(Screens.MAIN_MENU_SCREEN)
        }
    }

    private fun switchToErrorScreen() {
        viewModelScope.launch {
            _navigationFlow.tryEmit(Screens.ASSETS_LOADING_ERROR_SCREEN)
        }
    }
}